# Tugas Akhir 2017/2018 - Lampiran
Repository yang berisi semua lampiran yang diacu pada dokumen tugas akhir.
Setiap kelompok tugas akhir memiliki direktori kerja tersendiri dengan format penamaan SI-XX, di mana XX adalah nomor kelompok dalam dua digit angka. Di dalam direktori kerja dapat berisi subdirektori yang diperlukan, misalnya 'gambar', 'json', 'eksperimen', dsb.
--MSS